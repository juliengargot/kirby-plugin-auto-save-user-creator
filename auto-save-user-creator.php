<?php

kirby()->hook('panel.page.create', function($page) {
  $user = site()->user();
  try {
    $page->update(array(
      'author' => $user,
    ));
  } catch(Exception $e) {
    echo $e->getMessage();
  }
});
